FROM openjdk:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/products-catalog-0.0.1-SNAPSHOT-standalone.jar /products-catalog/app.jar

EXPOSE 8080

CMD ["java", "-jar", "/products-catalog/app.jar"]
